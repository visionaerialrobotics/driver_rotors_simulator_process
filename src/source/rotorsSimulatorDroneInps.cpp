/*!*******************************************************************************************
 *  \file       rotorsSimulatorDroneInps.cpp
 *  \brief      DroneCommandROSModule imlementation file.
 *  \details    This file implements the DroneCommandROSModule class.
 *  \authors    Ramon Suarez Fernandez, Hriday Bavle
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

//Drone
#include "rotorsSimulatorDroneInps.h"

// The commands comming from the midlevel controller needs to be muliplied by these values
// to covert the thrust commands in the range of -1 to +1
static double THRUST_SCALE_FULL_BATTERY = 1/32.681;
static double THRUST_SCALE_HALF_BATTERY = 1/29.71;
static double THRUST_SCALE_LOW_BATTERY  = 1/27.43;

////// DroneCommand ////////
DroneCommandROSModule::DroneCommandROSModule() :
    DroneModule(droneModule::active)
{
    init();
    return;
}

DroneCommandROSModule::~DroneCommandROSModule()
{
    return;
}

void DroneCommandROSModule::init()
{
    return;
}

void DroneCommandROSModule::close()
{
    return;
}

void DroneCommandROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //Configuration
    ros::param::get("~droneSwarmId", droneSwarmId);
    ros::param::get("~mav_name", mav_name);

    //Publishers
    drone_CtrlInput_publisher       = n.advertise<mav_msgs_rotors::RollPitchYawrateThrust>("/"+mav_name+std::to_string(droneSwarmId)+"/command/roll_pitch_yawrate_thrust", 1, true);
    //mavros_yaw_command_publisher    = n.advertise<geometry_msgs::Vector3Stamped>("mavros/yaw_command",1,true);

    //Subscribers
    ML_autopilot_command_subscriber = n.subscribe("command/low_level", 1, &DroneCommandROSModule::MLAutopilotCommandCallback, this);
    rotation_angles_subscriber      = n.subscribe("rotation_angles", 1, &DroneCommandROSModule::rotationAnglesCallback, this);
    battery_subscriber              = n.subscribe("battery",1, &DroneCommandROSModule::batteryCallback, this);
    hector_slam_subscriber          = n.subscribe("odometry/filtered", 1, &DroneCommandROSModule::hectorSlamCallback, this);
    yaw_ref_subscriber              = n.subscribe("droneControllerYawRefCommand", 1, &DroneCommandROSModule::yawReferenceCallback, this);
    Ekf_yaw_subscriber              = n.subscribe("EstimatedPose_droneGMR_wrt_GFF",1,&DroneCommandROSModule::ekfyawCallback, this);

    //Initialization
    yaw_angle = 1E10; //High value
    yaw_initial_mag = 1E10; //High value
    yawRef = 0;     //Current yaw
    update = true;

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool DroneCommandROSModule::resetValues()
{
    return true;
}

//Start
bool DroneCommandROSModule::startVal()
{
    return true;
}

//Stop
bool DroneCommandROSModule::stopVal()
{
    return true;
}

//Run
bool DroneCommandROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

void DroneCommandROSModule::yawReferenceCallback(const droneMsgsROS::droneYawRefCommand& msg){
    yawRef = msg.yaw;
    update = true;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void DroneCommandROSModule::MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    mav_msgs_rotors::RollPitchYawrateThrust attitude_msg;

    //mavros_msgs::AttitudeTarget    attitude_msg;
    //geometry_msgs::Vector3Stamped  yaw_command_msg;

    attitude_msg.header.stamp = ros::Time::now();
    //yaw_command_msg.header.stamp = ros::Time::now();

//    timePrev = timeNow;
//    timeNow = (double) ros::Time::now().sec + ((double) ros::Time::now().nsec / (double) 1E9);

    //yaw_command = (yaw_angle + (msg.dyaw * (0.06) )) * (M_PI/180.0); // Control the angle precisely

    // Debug
    //yaw_command = 90;



    //attitude_msg.yaw_rate = yaw_command;

    //if(attitude_msg.yaw_rate <=2*M_PI && attitude_msg.yaw_rate > M_PI)
        //attitude_msg.yaw_rate = attitude_msg.yaw_rate - 2*M_PI;
    //else if(attitude_msg.yaw_rate >=0 && attitude_msg.yaw_rate <= M_PI)
        //attitude_msg.yaw_rate = attitude_msg.yaw_rate;

   // attitude_msg.yaw_rate = - attitude_msg.yaw_rate;

    //std::cout << "Controlling yaw again with dYaw" << "  yaw_angle: " << yaw_angle * (M_PI/180.0) << "  dYaw: " << (msg.dyaw * (timeNow - timePrev)) * (M_PI/180.0) << std::endl;


    //convert the values in radians
    roll_command  = (+1)*msg.roll*(M_PI/180.0);
    pitch_command = (-1)*msg.pitch*(M_PI/180.0);
    yaw_command   = (-1)*msg.dyaw*(M_PI/180.0);
    //std::cout << "yaw_command " <<  yaw_command  << std::endl;

//    //convert euler angles to quaternion but in eigen form
//    quaterniond =  mavros::UAS::quaternion_from_rpy(roll_command, pitch_command, yaw_command);

//    //converting the NED frame to ENU for mavros
//    quaterniond_transformed = mavros::UAS::transform_orientation_ned_enu(mavros::UAS::transform_orientation_baselink_aircraft
//                                                                         (quaterniond));

//    tf::Quaternion q(quaterniond_transformed.x(), quaterniond_transformed.y(), quaterniond_transformed.z(), quaterniond_transformed.w());
//    //convert quaternion in eigen form to geometry_msg form
//    //tf::quaternionEigenToMsg(quaterniond_transformed, orientation);

//    tf::Matrix3x3 m(q);

//    //convert quaternion to euler angels
//    double y, p, r;
//    m.getEulerYPR(y, p, r);

    attitude_msg.pitch = pitch_command;
    attitude_msg.roll = roll_command;
    attitude_msg.yaw_rate = yaw_command;

    //choose the thrust values depending on the battery percentage
    //    if(battery_percent <= 100.0 && battery_percent >= 50.0)
    //       //converting thrust in the range of 0 to +1
    //      attitude_msg.thrust = msg.thrust * THRUST_SCALE_FULL_BATTERY;
    //    else if(battery_percent < 50.0 && battery_percent >= 0.0)
    //      attitude_msg.thrust = msg.thrust * THRUST_SCALE_HALF_BATTERY;
    //    else if(battery_percent < 0 && battery_percent >= -20.0)
    //      attitude_msg.thrust = msg.thrust * THRUST_SCALE_LOW_BATTERY;
    //    else
    attitude_msg.thrust.z = msg.thrust;

    drone_CtrlInput_publisher.publish(attitude_msg);
    //mavros_yaw_command_publisher.publish(yaw_command_msg);

    return;
}

void DroneCommandROSModule::rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
    yaw_angle = msg.vector.z;
    return;
}

void DroneCommandROSModule::batteryCallback(const droneMsgsROS::battery &msg)
{
    battery_percent = msg.batteryPercent;
    return;
}

void DroneCommandROSModule::hectorSlamCallback(const nav_msgs::Odometry &msg)
{
    //    geometry_msgs::PoseStamped vision_pose;

    //    vision_pose.header.stamp      = ros::Time::now();
    //    vision_pose.header.frame_id   = "odom";
    //    vision_pose.pose.position     = msg.pose.pose.position;
    //    vision_pose.pose.orientation  = msg.pose.pose.orientation;

    //    mavros_pose_publisher.publish(vision_pose);
}

void DroneCommandROSModule::ekfyawCallback(const droneMsgsROS::dronePose &msg)
{
    ekf_yaw = msg.yaw;
    if(ekf_yaw < 0) ekf_yaw += 2*M_PI;
}
