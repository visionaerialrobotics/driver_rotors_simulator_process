/*!*********************************************************************************
 *  \file       rotorsSimulatorDroneInps.h
 *  \brief      DroneCommandROSModule definition file.
 *  \details    This file contains the DroneCommandROSModule declaration. To obtain more information about
 *              it's definition consult the rotorsSimulatorDroneInps.cpp file.
 *  \authors    Ramon Suarez Fernandez, Hriday Bavle
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

#include <iostream>
#include <math.h>
#include <cmath>

//tf messages
#include <tf/transform_datatypes.h>

//// ROS  ///////
#include "ros/ros.h"

#include "droneModuleROS.h"
#include "communication_definition.h"
#include "droneMsgsROS/droneAutopilotCommand.h"
#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/dronePose.h"

//geometry msgs
#include "geometry_msgs/TwistStamped.h"
#include "geometry_msgs/PoseStamped.h"

//Navmsg
#include "nav_msgs/Odometry.h"

//mav_msgs_rotors
#include "mav_msgs_rotors/RollPitchYawrateThrust.h"
#include "eigen_conversions/eigen_msg.h"

//standard messages ROS
#include "std_msgs/Float64.h"
#include "tf/transform_datatypes.h"
#include "eigen_conversions/eigen_msg.h"
#include "Eigen/Core"
#include "Eigen/Geometry"
#include "tf_conversions/tf_eigen.h"
#include <cmath>

//Mavros
#include "mavros_msgs/AttitudeTarget.h"
#include "mavros_msgs/ActuatorControl.h"
#include "mavros_msgs/State.h"
#include "mavros/mavros_uas.h"
#include <pluginlib/class_list_macros.h>

#include <mavros/mavros_plugin.h>
#include <mavros/setpoint_mixin.h>

#define ANGLE_TOLERANCE 0.0872664626    // 5 degrees in rad


/////////////////////////////////////////
// Class DroneCommand
//
//   Description
//
/////////////////////////////////////////
class DroneCommandROSModule : public DroneModule
{
    //Constructors and destructors
public:
    DroneCommandROSModule();
    ~DroneCommandROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);
    void sendSetpoints();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

protected:
    //Subscribers
    ros::Subscriber ML_autopilot_command_subscriber;
    ros::Subscriber rotation_angles_subscriber;
    ros::Subscriber battery_subscriber;
    ros::Subscriber hector_slam_subscriber;
    ros::Subscriber yaw_ref_subscriber;
    ros::Subscriber Ekf_yaw_subscriber;
    void MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg);
    void rotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg);
    void batteryCallback(const droneMsgsROS::battery& msg);
    void hectorSlamCallback(const nav_msgs::Odometry& msg);
    void yawReferenceCallback(const droneMsgsROS::droneYawRefCommand& msg);
    void ekfyawCallback(const droneMsgsROS::dronePose& msg);
    // Publishers
    ros::Publisher drone_CtrlInput_publisher;
    ros::Publisher drone_setpoint_attitude_throttle_publ;
    ros::Publisher mavros_yaw_command_publisher;

public:
    std::string mav_name;
    int droneSwarmId;
    std::vector<geometry_msgs::PoseStamped> poses;
    geometry_msgs::Quaternion orientation;
    Eigen::Quaterniond quaterniond, quaterniond_transformed;
    double roll_command, pitch_command, yaw_command, yaw_angle;
    double roll, pitch, yaw;
    double battery_percent;
    double yawRef, yaw_initial_mag, ekf_yaw;
    double timePrev, timeNow;
    bool update;

};
