/*!*********************************************************************************
 *  \file       rotorsSimulatorDroneOuts.h
 *  \brief      RotationAnglesROSModule definition file.
 *  \details    This file contains the RotationAnglesROSModule declaration. To obtain more information about
 *              it's definition consult the rotorsSimulatorDroneOuts.cpp file.
 *  \authors    Ramon Suarez Fernandez, Hriday Bavle
 *  \copyright  Copyright (c) 2019 Universidad Politecnica de Madrid
 *              All rights reserved
 *
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ********************************************************************************/

//ROS
#include "ros/ros.h"

//math library
#include "math.h"

#include "droneModuleROS.h"
#include "communication_definition.h"

//IMU
#include "sensor_msgs/Imu.h"

//Odometry
#include "nav_msgs/Odometry.h"

//Rotaion Angles
#include "geometry_msgs/Vector3Stamped.h"
#include "geometry_msgs/TwistStamped.h"

//Battery
#include "droneMsgsROS/battery.h"

//Altitude
#include "droneMsgsROS/droneAltitude.h"
#include "sensor_msgs/Range.h"

//Speeds
#include "droneMsgsROS/vector2Stamped.h"

//Mavros_msgs
#include "mavros_msgs/BatteryStatus.h"
#include "mavros/mavros_uas.h"
#include <pluginlib/class_list_macros.h>
#include <cmath>

//tf messages
#include <tf/transform_datatypes.h>

//Eigen
#include "eigen_conversions/eigen_msg.h"

// low pass filter
#include "control/LowPassFilter.h"
#include "xmlfilereader.h"
#include "control/filtered_derivative_wcb.h"

#include <gazebo_msgs/ModelStates.h>

#include <Eigen/Dense>

#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"

#define NoAltitudeFiltering
//#define AltitudeFiltering

class RotationAnglesROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher RotationAnglesPubl;
    ros::Publisher RotationAnglesRadiansPub;
    ros::Publisher ImuDataPubl;
    bool publishRotationAnglesValue(const geometry_msgs::Vector3Stamped &RotationAnglesMsgs);
    bool publishImuData(const sensor_msgs::Imu &ImuData);
    ros::Time lastTimeRotation;


    //Subscriber
protected:
    ros::Subscriber RotationAnglesSubs;
   void rotationAnglesCallback(const sensor_msgs::Imu &msg);

    //RotationAngles msgs
protected:
//    geometry_msgs::Vector3Stamped RotationAnglesMsgs;


    //Constructors and destructors
public:
    RotationAnglesROSModule();
    ~RotationAnglesROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

public:
    std::string mav_name;
    int droneSwarmId;
    double roll,pitch,yaw;
    double roll_rad, pitch_rad, yaw_rad;
    Eigen::Quaterniond quaterniond;
    geometry_msgs::Quaternion quaternion;

};

//Battery Class
class BatteryROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher BatteryPubl;
    bool publishBatteryValue(const droneMsgsROS::battery &BatteryMsgs);


    //Subscriber
protected:
    ros::Subscriber BatterySubs;
    void batteryCallback(const mavros_msgs::BatteryStatus &msg);


    //Battery msgs
protected:
//    droneMsgsROS::battery BatteryMsgs;


    //Constructors and destructors
public:
    BatteryROSModule();
    ~BatteryROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};

//---Altitude given by the lidarlite --------------//
class AltitudeROSModule : public DroneModule
{
//Publisher
protected:
    ros::Publisher AltitudePub;
    bool publishAltitude(const droneMsgsROS::droneAltitude altitudemsg);


    //Subscriber
protected:
    ros::Subscriber AltitudeSub;
    ros::Subscriber AltitudeFilteredSub;
    void publishAlitudeCallback(const nav_msgs::Odometry &msg);
    void publishAltitudeFilteredCallback(const geometry_msgs::PoseStamped &msg);

protected:
    droneMsgsROS::droneAltitude AltitudeMsg;
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb;

    //Constructors and destructors
public:
    AltitudeROSModule();
    ~AltitudeROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    std::string mav_name;
    int droneSwarmId;
    bool run();


};

//----------------Local Pose given by Mavros------------//

class LocaPoseROSModule : public DroneModule
{
    //Publisher
protected:
    ros::Publisher localPoseAltitudePublisher;
    bool publishLocalPoseAltitude(const droneMsgsROS::droneAltitude &altitudemsg);
    droneMsgsROS::droneAltitude AltitudeMsg;

    ros::Publisher localPoseSpeedsPublisher;
    ros::Publisher localPosePublisher;
    ros::Publisher localSpeedPublisher;
    bool publishLocalPoseSpeeds(const droneMsgsROS::vector2Stamped &speedsmsg);
    droneMsgsROS::vector2Stamped SpeedsMsg;
    droneMsgsROS::dronePose gazeboPoseMsg;
    droneMsgsROS::droneSpeeds gazeboSpeedsMsg;

    ros::Time lastTimePose;

    //Subscriber
protected:
    ros::Subscriber mavrosLocalPoseSubsriber;
    void localPoseAltitudeCallback(const nav_msgs::Odometry &msg);
    ros::Subscriber mavrosLocalSpeedsSubsriber;
    void localSpeedsCallback(const geometry_msgs::TwistStamped &msg);
    void localSpeedsCallbackGazebo(const nav_msgs::Odometry &msg);

protected:
    CVG_BlockDiagram::FilteredDerivativeWCB filtered_derivative_wcb;

    //Constructors and destructors
public:
    LocaPoseROSModule();
    ~LocaPoseROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

public:
    std::string mav_name;
    int droneSwarmId;

   // double roll,pitch,yaw;

};
